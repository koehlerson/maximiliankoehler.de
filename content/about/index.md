+++
date = "2016-11-05T21:05:33+05:30"
title = "About"
+++
## Maximilian Köhler

{{< figure src="profile_picture_transparent_1500.png" title="It'se me" class="profilepic">}}

### Publications

 - [An enhanced algorithmic scheme for relaxed incremental variational damage formulations at finite strains](https://onlinelibrary.wiley.com/doi/full/10.1002/pamm.202100135)
 - [Continuum multiscale modeling of absorption processes in micro- and nanocatalysts](https://link.springer.com/article/10.1007/s00419-022-02172-8)
 - [Simulation of absorption processes in nanoparticle catalysts](https://onlinelibrary.wiley.com/doi/full/10.1002/pamm.202000076)

### Education
 - **2017-2020** Computational Engineering Ruhr - University Bochum
 - **2014-2017** Civil Engineering Bauhaus - University Weimar
 
### Working experience 
 - **2020-now** Research Associate, Chair of Continuum Mechanics
 - **2018-2020** Undergraduate Research Assistant, Chair of Continuum Mechanics
 - **2019-2020** Undergraduate Research Assistant, High Performance Computing in the Engineering Sciences
 - **2019-2019** Guest Student @ GSP Juelich
 - **2017-2018** Project engineer, Office for structural design, Bochum
 - **2016-2017** Working Student, Office for structural design, Weimar

### Coding Experience
 - **Julia** - [Ferrite.jl](https://github.com/Ferrite-FEM/Ferrite.jl), [FerriteViz.jl](https://github.com/Ferrite-FEM/FerriteViz.jl), [FerriteGmsh.jl](https://github.com/Ferrite-FEM/FerriteGmsh.jl)
 - **Python** - FEM Framework, FEniCS, fastai, PyTorch, keras
 - **C++** - FEM
 - **Hugo** - This website

## License

Copyright © 2020 [Marcin Mierzejewski](https://mrmierzejewski.com/)

The theme is released under the MIT License. Check the [original theme license](https://github.com/panr/hugo-theme-terminal/blob/master/LICENSE.md) for additional licensing information.


