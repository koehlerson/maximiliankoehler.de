+++
title = "Advances in Relaxed Continuum Damage Mechanics at Finite Strains: Adaptive Convexification and Strain Softening"
publishdate = "2023-01-01"
date = "2023-01-27"
location = "Vienna, Austria"
conference = "22nd GAMM Seminar on Microstructures"
conferencelink = "https://www.asc.tuwien.ac.at/gamm2023/"
+++
