+++
title = " Multidimensional Rank-One Convexification of Incremental Damage Formulations: Algorithmic Treatment, Implementation Aspects, and Numerical Analysis "
publishdate = "2022-07-29"
date = "2022-06-07"
location = "Oslo, Norway"
conference = "8th European Congress on Computational Methods in Applied Sciences and Engineering"
conferencelink = "https://www.eccomas2022.org/"
+++
