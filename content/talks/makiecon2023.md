+++
title = "Visualizing PDE Solutions on Unstructured Meshes with Makie.jl"
publishdate = "2023-04-19"
date = "2023-04-19"
location = "Jena, Germany"
conference = "MakieCon 2023"
conferencelink = "https://youtu.be/dMQ-CCw9Lbk"
+++
