+++
title = "Hello There"
date = "2019-07-04"
+++

Hey fella, small website about my science and photography stuff. In case you're looking for information about my working experiences,
please check out the [about](/about/) section. Otherwise, feel free to contact me for coding/engineering or photography related projects. Also, catch a glimpse of 
the photo gallery in the [photos](/photos/) section.

