+++
image = "imm013_12A.jpg"
date = "2020-10-06"
title = "Bochum, GER"
type = "gallery"
+++

In case you visit or live in Bochum, please consider to visit this guy's restaurant. 
It's called Himalaya Imbiss.
I walked by his shop when he came out and this dude spreads such positive vibes that I immediately had to ask for a portrait. It's one of my best shots, tbh. 

* Canon AE-1, 50mm 1.4
* Kodak Gold 200
